Pixels Watchface
==================

![img2.jpg](https://bitbucket.org/repo/GyXL7M/images/2380286862-img2.jpg)

This is my first attempt at creating something compatible with Android Wear. The idea is creating a minimalistic watchface for round devices (like Moto 360), and is not fully tested or completely ended. More like a WIP for trying Wear SDK and adding more features to the watchface.

The project is based on TwoToasters' [Android Watchface Template](https://github.com/twotoasters/watchface-template).